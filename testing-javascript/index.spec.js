const assert = require('assert')
const { add } = require('./index')

console.log('First test');
const actual = add(5, 6)
const expectation = 11
assert.equal(actual, expectation)
console.log('Successfully run all tests!')
console.log('End of first test');

console.log('Second test: test that add function throws an error if no arguments are passed');
assert.throws(() => {
  add()
})
console.log('Successfully run all tests!')
console.log('End of first test');
